require "httpi"
require "json"

module Zapcar
  class API
    attr_reader :request

    def initialize
      @request = HTTPI::Request.new
    end

    def add_items(item_ids)
      request.url = "http://api.vitocars.net/api/Record/Write"
      items = Item.where(id: item_ids)
      items.each do |item|
        brand = $db[:brand].where(title: item.brand.strip).first
        category = $db[:category].where(title: item.category.strip).first
        modelus = $db[:modelus].where(title: item.modelus.strip).first
        part_name = $db[:part].where(title: item.part.strip).first
        if !brand.nil? && !category.nil? && !modelus.nil? && !part_name.nil?
          i = {part_unique: item.art,
           brand_id: brand[:vitocars_id],
           model_id: modelus[:vitocars_id],
           category_id: category[:vitocars_id],
           part_name_id: part_name[:vitocars_id],
           price: item.price,
           description: item.title,
           quantity: 1,
           is_new: item.used ? 0 : 1}
        end
        puts i.to_json
        call({data: i.to_json})
      end
    end

    def delete_items(ids)
      request.url = "http://api.vitocars.net/api/RegisterOperation/Sale"
      items = Item.where(id: ids)
      items.each do |item|
        i = {part_unique: item.art, 
             quantity: 1}
        puts i.to_json
        call({data: i.to_json})
      end
    end

    def add_pictures(pictures_ids)
      request.url = "http://api.vitocars.net/api/Photo"
      pictures = Picture.where(id: pictures_ids)
      pictures.each do |picture|
        item = picture.item
        unless item.nil? || item.vitocars_id.nil?
          i = "{\"part_id\":\""+item.vitocars_id.to_s+"\",\"photo_url\":\""+picture.url+"\"}"
          call({data: i})
        end
      end
    end

    def call(query = {})
      request.query = {token_id: "YB0BuBeFqSBm1Qle93Je8BQ7X", 
                       lang: "ru"}.merge(query)
      resp = HTTPI.post(request)
      puts request.url
      JSON.parse(resp.body)["data"] if resp.code == 200
    end
  end
end