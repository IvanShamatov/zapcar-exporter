require "bundler"
Bundler.require
require_relative "zapcar.rb"

namespace :db do
  task :create do 
    $db.create_table :items do 
      primary_key :id
      String :art, unique: true
      String :category
      String :part
      String :brand
      String :modelus
      String :manufacture
      String :comment
      Float  :price
      String :title
      Time :updated_at
      Boolean :used
      Boolean :uploaded
      Integer :quantity
      String  :vitocars_id
    end
    puts "Table Items created"

    $db.create_table :pictures do
      primary_key :id
      foreign_key :item_id, :items
      String :url
    end
    puts "Table Pictures created"

    [:category, :part, :brand, :modelus].each do |table|
      $db.create_table table do 
        String :title, index: true
        Integer :vitocars_id
      end
      puts "Table #{table} created"
    end
  end

  task :drop do 
    [:pictures, :items, :category, :part, :brand, :modelus].each do |table|
      $db.drop_table table
    end  
  end

  task :seed do 
    file = CSV.read("dicts/final_list.csv", :col_sep => ";")
    file.each do |f|
      $db[:part].insert(title: f[1], vitocars_id: f[2])
    end
    puts "Table part seeded"

    file = CSV.read("dicts/final_cat.csv", :col_sep => ";")
    file.each do |f|
      $db[:category].insert(title: f[1], vitocars_id: f[0])
    end
    puts "Table category seeded"

    file = CSV.read("dicts/final_brands.csv", :col_sep => ";")
    file.each do |f|
      $db[:brand].insert(title: f[0], vitocars_id: f[1])
    end
    puts "Table brand seeded"

    file = CSV.read("dicts/final_models.csv", :col_sep => ";")
    file.each do |f|
      $db[:modelus].insert(title: f[1], vitocars_id: f[2])
    end
    puts "Table modelus seeded"    
  end

end


namespace :zapcar do 
  task :run do 
    Zapcar.run!
  end

  task :pictures do 
    Zapcar.pics!
  end
end