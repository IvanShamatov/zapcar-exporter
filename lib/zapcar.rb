require "bundler"
Bundler.require
require "csv"
require_relative "initializer.rb"
require_relative "models.rb"
require_relative "api.rb"


module Zapcar


  def self.run!  
    # download new prices
    file = download_csv
    
    new_items = []
    new_pictures = []
    update_items = []
    csv = CSV.read(file, :headers => true, :col_sep => ";")
    csv.each do |line|
      unless line[0].empty?
        old = Item.where(art: line[0]).first
        # if old item with same artic found
        if old.nil?
          item_id = Item.insert(art: line[0],
                             category: line[1], #
                             part: line[2], #
                             brand: line[3], #
                             modelus: line[4], #
                             manufacture: line[5],
                             comment: line[6],
                             price: line[7].to_f,
                             title: line[8],
                             updated_at: Time.now,
                             used: line[10].to_i == 0 ? true : false,
                             quantity: line[9].to_i)
          pictures = Picture.get_from(line, item_id)
          new_pictures = new_pictures + pictures
          new_items << item_id
        else 
          old.updated_at = Time.now
          old.save
        end
      end
    end
    delete_items = Item.where("updated_at < ? AND quantity > 0", Time.now - 24*60*60)
    delete_items.update(quantity: 0)
    ids = delete_items.map(:id)


    # upload updates and new
    api = API.new
    puts "Adding new items"
    api.add_items new_items unless new_items.empty?
    puts "Adding new pictures"
    api.add_pictures new_pictures unless new_pictures.empty?
    puts "Deleting items"
    api.delete_items ids unless ids.empty?

    compress_and_remove(file)
  end

  def self.pics!
    api = API.new
    pictures = Picture.select(:id)
    pictures.inspect
    api.add_pictures pictures
  end


  def self.download_csv
    request = HTTPI::Request.new
    request.url = "http://www.zapcar.ru/_upload/export.csv"
    response = HTTPI.get(request)
    File.write(File.join("export", Date.today.strftime("%y%m%d.csv")), 
                response.body.force_encoding(Encoding::WINDOWS_1251).encode!(Encoding::UTF_8))
    File.join("export", Date.today.strftime("%y%m%d.csv"))
  end


  def self.compress_and_remove( filename )
    Zlib::GzipWriter.open(filename+".gz") do |gz|
      gz.write File.read(filename)
    end
    FileUtils.rm filename
  end
end