module Zapcar

  class Item < Sequel::Model
    one_to_many :pictures
  end

  

  class Picture < Sequel::Model
    many_to_one :item

    def self.get_from(line, item_id)
      [*11..16].select {|i| !line[i].nil? && !line[i].empty?}.map {|i| Picture.insert(url: line[i], item_id: item_id) }
    end
  end
  Picture.set_dataset(:pictures)
end