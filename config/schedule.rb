job_type :rake,    "cd :path && bundle exec rake :task --silent :output"

every :day, at: "3:00am" do 
  rake "zapcar:run"
end